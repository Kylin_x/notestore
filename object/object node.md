## object方法

1. Object.assign ( 目标对象 , 源对象1 , 源对象2 ,...) 对象的合并，将源对象的所有可枚举属性合并到目标对象上【只拷贝源对象的自身属性，不考虑继承的属性】

   举例：

   ```js
   const target = {x:0,y:1};
   const source = {x:1,z:2,fn:{number:1}};
   console.log(Object.assign(target,source));
   //target {x:1,y:1,z:2,fn:{number:1}} 同名属性会被覆盖
   
   Object.assign([1,2,3],[4,5])
   // [4,5,3]  可以处理数组，但会把数组当作对象的处理
   ```

2. Object.create ( 对象 , 新的对象 ) 使用指定的原型对象及其属性去创建一个新的对象

   举例：

   ```js
   var parent={x:1,y:1};
   var child=Object.create(parent,{
       z:{
         writable:true,
         configurable:true,
         value:'newObj',
       }
   });
   console.log(child)//{Z:'newObj'}
   console.log(child.x)//1
   ```
   
3. Object.defineProperties ( obj , props ) 直接在一个对象上定义新的属性或修改现有属性，并返回该对象

   举例：

   ```js
   var obj={};
   Object.defineProperties(obj,{
       'property1':{
           value:true,
           writable:true
       },
       'property2':{
           value:'Hello',
           writable:false
       }
   })
   console.log(obj)//{property1:true,property2:'Hello'}
   ```
   
4. Object.defineProperty ( obj , prop , descriptor ) 载一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回这个对象

   举例：

   ```
   Object.defineProperty(Object,'is',{
     value:function(x,y){
         if(x===y){
           return x !==0 || 1/x === 1/y
         }
         return x !== x && y !== y;
     },
     configurable:true,
     enumerable:false,
     writable:true
   })
   ```
   
5. Object.keys (obj) 返回一个由一个给定对象的自身可枚举属性组成的数组，数组中属性名的排列顺序和使用for...in循环遍历该对象时返回的顺序一致（两者的主要区别是一个for-in循环还会枚举其原型链上的属性）
   
   举例：
   
   ```js
   /*数组*/
   var arr = ['a','b','c'];
   console.log(Object.keys(arr));//['0','1','2']
   /*对象*/
   var obj = {foo:'bar',baz:42};
   keys = Object.keys(obj);
   console.log(keys);//['foo','baz']
   ```
   
6.  Object.values (obj) 返回一个给定对象自己的所有可枚举属性值的数组，值的顺序与使用for...in循环的顺序相同（区别在于for-in循环枚举原型链中的属性）。【会过滤属性名为Symbol值的属性】

   举例：

   ```js
   var an_obj = {100:'a',2:'b',7:'c'};
   console.log(Object.values(an_obj));//['b','c','a']
   
   var obj = {0:'a',1:'b',2:'c'};
   console.log(Object.values(obj));//['a','b','c']
   ```
   
7. Object.entries (obj) 返回一个给定对象自身可枚举属性的键值对数组，其排列与使用for...in循环遍历该对象时返回的顺序一致（区别在于for-in循环也枚举原型链中的属性）

   举例：

   ```js
   const obj = {foo:'bar',baz:42};
   console.log(Object.entries(obj));//[['foo','bar'],['baz',42]]
   const simuArray = {0:'a',1:'b',2:'c'};
   console.log(Object.entries(simuArray));//[['0','a'],['1','b'],['2','c']]
   ```
8. Object.hasOwnProperty()判断对象自身属性中是否具有指定的属性。
9. Object.getOwnPropertyNames()返回一个由指定对象的所有自身属性的属性名（包括不可枚举属性但不包括Symbol值作为名称的属性）组成的数组。

   举例：

```js
var obj= { 0:"a",1:"b",2:"c"}
Object.getOwnPropertyNames(obj).forEach(function(val){
	console.log(val)
})
var obj={
	x:1,
	y:2
}
Object.defineProperty(obj,'z',{
	enumerable:false
})
console.log(Object.getOwnPropertyNames(obj))//['x','y','z']包含不可枚举属性
console.log(Object.keys(obj))// ["x","y"]只包含可枚举属性

```
10. isPrototypeOf () 判断一个对象是否存在于另一个对象的原型链上。

11. Object.setPrototypeOf ( obj , prototype ) 设置对象的原型对象。

12. Object.is () 判断两个值是否相同。
    两个值相同的情况：
    都是undefined
    都是null
    都是true
    都是false
    都是由相同个数的字符按照相同的顺序组成的字符串
    都是指向同一个对象
    都是正数
    都是负数
    都是NaN
    都是同一个数字

    举例：

    ```js    
    Object.is('foo', 'foo');     // true
    Object.is(window, window);   // true
    
    Object.is('foo', 'bar');     // false
    Object.is([], []);           // false
    
    var test = { a: 1 };
    Object.is(test, test);       // true
    
    Object.is(null, null);       // true
    
    // 特例
    Object.is(0, -0);            // false
    Object.is(-0, -0);           // true
    Object.is(NaN, 0/0);         // true
    ```

13. Object.freeze ()冻结一个对象，不能想这个对象添加新的属性，不能修改、删除其已有属性，不能修改该对象已有属性的可枚举性、可配置性、可写性，这个对象永远是不可变的。该方法返回被冻结的对象。

    举例：

    ```js
    var obj = {
      prop: function() {},
      foo: 'bar'
    };
    
    // 新的属性会被添加, 已存在的属性可能
    // 会被修改或移除
    obj.foo = 'baz';
    obj.lumpy = 'woof';
    delete obj.prop;
    
    // 作为参数传递的对象与返回的对象都被冻结
    // 所以不必保存返回的对象（因为两个对象全等）
    var o = Object.freeze(obj);
    
    o === obj; // true
    Object.isFrozen(obj); // === true
    
    // 现在任何改变都会失效
    obj.foo = 'quux'; // 静默地不做任何事
    // 静默地不添加此属性
    obj.quaxxor = 'the friendly duck';
    console.log(obj)
    ```

14. Object.isFrozen () 判断一个对象是否被冻结。

15. Object.preventExtensions () 对象不能再添加新的属性。可修改，删除现有属性，不能添加新属性。

    举例：

    ```js
    var obj = {
      name :'lilei',
      age : 30 ,
      sex : 'male'
    }
    
    obj = Object.preventExtensions(obj);
    console.log(obj);    // {name: "lilei", age: 30, sex: "male"}
    obj.name = 'haha';
    console.log(obj)     // {name: "haha", age: 30, sex: "male"}
    delete obj.sex ;
    console.log(obj);    // {name: "haha", age: 30}
    obj.address  = 'china';
    console.log(obj)     // {name: "haha", age: 30}
    ```

    

