# ES6
## let const
1. let: 声明变量，无变量提升，块级作用域，暂时性死区，同一作用域不允许重复声明。
2. const: 声明常量且不能改变，无变量提升，块级作用域，暂时性死区。

## 解构赋值
  详情请戳"https://es6.ruanyifeng.com/#docs/destructuring"
  定义：ES6 允许按照一定模式，从数组和对象中提取值，对变量进行赋值，这被称为解构。
  1. 数组的解构赋值
    ```js
    // 1.简单赋值
    let[a,b,c] = [1,2,3];
    console.log(a,b,c)
    // 1 2 3

    // 2.多维数组解构赋值
    let [a,[b],[[c]]] = [1,[2],[[3]]];
    console.log(a,b,c)
    // 1 2 3

    // 3.默认值 只有当右边对应位置为undefined时候才会选择默认（null不属于undefined）
    let [a=1,b=2,c=3,d]=[undefined,null,'C','D'];
    console.log(a,b,c,d)
    // 1 null 'C' 'D'

    // 4..左右不对等,会相应的对号入座，没有的以undefined赋值
    // 左边多
    let [a,b,c,d] = [1,2,3];
    console.log(a,b,c,d);
    // 1,2,3 undefined
    // 右边多
    let [a,b,c] = [1,2,3,4];
    console.log(a,b,c);
    // 1,2,3
    ```
  2. 对象的解构赋值
    ```js
    // 普通赋值，对象右边的顺序可以打乱
    let {foo,bar} = {foo：'zhangSan',bar: 'liSi'};
    console.log(foo,bar);
    // zhangSan liSi

    // 默认值赋值，同数组
    let {foo=1,bar=2} = {bar='liSi'};
    console.log(foo,bar);
    // 1, liSi

    // 变量名和属性名不一致
    let {obj:name} = {obj: 'laoxie'};
    console.log(name);
    // laoxie
    let ob = { name: 'jingmingju',job: 'student'};
    console.log(name,job);
    // jingmingju student
    ```
  3. 字符串的解构
    ```js
    let [a,b,c,d,e]='hello';
    console.log(a,b,c,d,e);
    // h e l l o
    let {length:len} = 'hello';
    console.log(len);
    // 5
    ```

## 模板字符串
  - 定义：模板字符串（template string）是增强版的字符串，用反引号（`）标识。它可以当作普通字符串使用，也可以用来定义多行字符串，或者在字符串中嵌入变量。
  ```js
    // 普通字符串
    `hello`
    // 多行字符串
    `举头望明月
    低头思故乡`
    // 字符中嵌入变量
    let name = 'Bob',time='today';
    'Hello ${name}, how are you ${tody}`
  ```

## 字符串新增方法
  - includes()：返回布尔值，表示是否找到了参数字符串。
  - startsWith()：返回布尔值，表示参数字符串是否在原字符串的头部。
  - endsWith()：返回布尔值，表示参数字符串是否在原字符串的尾部。
  注意：这三个方法都支持第二个参数，表示开始搜索的位置。

  - repeat方法返回一个新字符串，表示将原字符串重复n次。
  注意：参数如果是小数，会被取整。如果repeat的参数是负数或者Infinity，会报错。参数是0到-1之间的小数则等同于0，因为会先进行取整运算。参数NAN等同于0。如果repeat的参数是字符串则会先转成数字。
  
  - padStart()用于头部补全。
  - padEnd()用于尾部补全。
  注意：
    1.padStart()和padEnd()一共接受两个参数，第一个参数是字符串补全生效的最大长度，第二个参数是用来补全的字符串。
    2.如果原字符串的长度，等于或大于最大长度，则字符串补全不生效，返回原字符串。
    3.如果用来补全的字符串与原字符串，两者的长度之和超过了最大长度，则会截去超出位数的补全字符串。
    4.如果省略第二个参数，默认使用空格补全长度。

  - trimStart()消除字符串头部的空格。
  - trimEnd()消除字符串尾部的空格。
  注意：
    1.trimStart()只消除头部的空格，保留尾部的空格。trimEnd()也是类似行为。
    2.除了空格键，这两个方法对字符串头部（或尾部）的 tab 键、换行符等不可见的空白符号也有效。
    3.浏览器还部署了额外的两个方法，trimLeft()是trimStart()的别名，trimRight()是trimEnd()的别名。

  - matchAll()方法返回一个正则表达式在当前字符串的所有匹配。
  - replaceAll()一次性替换所有匹配，用法与replace()相同，返回一个新字符串，不会改变原字符串。

## 数值的扩展
  - Number.isFinite()用来检查一个数值是否为有限的（finite），即不是Infinity。
  - Number.isNaN()用来检查一个值是否为NaN。
  注意：
    1.如果参数类型不是NaN，Number.isNaN一律返回false。
    2.Number.isFinite()对于非数值一律返回false, Number.isNaN()只有对于NaN才返回true，非NaN一律返回false。

  - Number.parseInt(), Number.parseFloat()。ES6 将全局方法parseInt()和parseFloat()，移植到Number对象上面，行为完全保持不变。这样做的目的，是逐步减少全局性方法，使得语言逐步模块化。

  - Number.isInteger()用来判断一个数值是否为整数。
  注意：
    1.JavaScript 内部，整数和浮点数采用的是同样的储存方法，所以 25 和 25.0 被视为同一个值。
    2.如果参数不是数值，Number.isInteger返回false。

  - Number.EPSILON，ES6 在Number对象上面，新增一个极小的常量Number.EPSILON。根据规格，它表示 1 与大于 1 的最小浮点数之间的差。

  - 安全整数和 Number.isSafeInteger()。JavaScript 能够准确表示的整数范围在-2^53到2^53之间（不含两个端点），超过这个范围，无法精确表示这个值。ES6 引入了Number.MAX_SAFE_INTEGER和Number.MIN_SAFE_INTEGER这两个常量，用来表示这个范围的上下限。Number.isSafeInteger()则是用来判断一个整数是否落在这个范围之内。

  Math对象的扩展
    ES6 在 Math 对象上新增了 17 个与数学相关的方法。所有这些方法都是静态方法，只能在 Math 对象上调用。
    - Math.trunc方法用于去除一个数的小数部分，返回整数部分。
    注意：
      1.对于非数值，Math.trunc内部使用Number方法将其先转为数值。
      2.对于空值和无法截取整数的值，返回NaN。
    
    - Math.sign方法用来判断一个数到底是正数、负数、还是零。对于非数值，会先将其转换为数值。它会返回五种值。参数为正数，返回+1；参数为负数，返回-1；参数为 0，返回0；参数为-0，返回-0;其他值，返回NaN。
    注意：如果参数是非数值，会自动转为数值。对于那些无法转为数值的值，会返回NaN。

    - Math.cbrt()方法用于计算一个数的立方根。
    注意：对于非数值，Math.cbrt()方法内部也是先使用Number()方法将其转为数值。

    - Math.hypot方法返回所有参数的平方和的平方根。如果参数不是数值，Math.hypot方法会将其转为数值。只要有一个参数无法转为数值，就会返回 NaN。

  指数运算符
    ES2016 新增了一个指数运算符（**）。这个运算符的一个特点是右结合，而不是常见的左结合。多个指数运算符连用时，是从最右边开始计算的。
    ```js
    2 ** 2 // 4
    2 ** 3 // 8
    2 ** 3 ** 2 // 相当于2 ** (3 ** 2)，最后的值为512
     ```

## 函数的扩展
  