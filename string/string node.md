# JavaScript 字符串

- JavaScript 字符串用于存储和处理文本
- 字符串可以存储一系列字符，如'Shaorui Yvan'
- 字符串可以是插入到引号中的任何字符，可以使用单引号也可以使用双引号
- 可以使用索引位置访问字符串中的每个字符

## 字符串方法

### 查找字符串中的字符串
- indexOf() 返回字符串中指定文本首次出现的索引
- 例子
```js
    var str='The full name of China is the People's Republic of China.';
    var pos=str.indexOf('China')
    console.log(pos)//结果为17
```
-  lastIndexOf() 返回指定文本在字符串中最后一次出现的索引
-  例子
```js
	var str='The full name of China is the People's Republic of China.';
	var pos=str.lastIndexOf('China');
	console.log(pos)//结果为51
```
- 如果未找到文本，indexOf()和lastIndexOf()均返回-1.
- 例子
```js
	var str='The full name of China is the People's Republic of China.';
	var pos=str.indexOf('USA');
	console.log(pos)//结果为-1
```
- 两种方法都接受作为检索起始位置的第二个参数
- 例子
```js
	//indexOf()
	var str='The full name of China is the People's Republic of China.';
	var pos = str.indexOf("China",18);
	console.log(pos)//结果为51
	//lastIndexOf() 方法向后进行检索（从尾到头），这意味着：假如第二个参数是 50，则从位置 50 开始检索，直到字符串的起点。
	var str='The full name of China is the People's Republic of China.';
	var pos = str.lastIndexOf("China",50);
	console.log(pos)//结果为17
```

### 检索字符串中的字符串
- search() 搜索特定值的字符串，并返回匹配的位置
- 例子
```js
	var str='The full name of China is the People's Republic of China.';
	var pos=str.search('locate');
	console.log(pos)//结果为17
```
-- search()和indexOf()区别在于indexOf()可以设置更强大的搜索值。

### 提取部分字符串
- slice() 提取字符串的某个部分并在新字符串中返回被提取的部分。有两个参数：起始索引(开始位置)，终止索引(结束位置)。
- 例子
```js
	var str='Apple,Banana,Mango';
	var res=str.slice(7,13);
	console.log(res);//结果为Banana
	//如果某个参数为负，则从字符串的结尾开始计数。
	var res=str.slice(-13,-7);
	console.log(res);//结果为Banana
	//如果省略第二个参数，则该方法将裁剪字符串的剩余部分；
	var res=str.slice(7);
	console.log(res);//结果为Banana, Mango
	//或者从结尾计数；
	var res=str.slice(-13);
	console.log(res);//结果为Banana, Mango
```
- substring() 类似于slice(),唯一不同的是substring()无法接受负的索引。
- 例子
```js
	var str='Apple,Banana,Mango';
	var res=str.slice(7,13);
	console.log(res);//结果为Banana
	//如果省略第二个参数，则该 substring() 将裁剪字符串的剩余部分。
	var res=str.slice(7);
	console.log(res);//结果为Banana, Mango
```
- substr()类似于slice(),不同之处在于第二个参数规定被提取部分的长度,第二个参数不能为负，因为它定义的是长度。

### 替换字符串内容
- replace()用另一个值替换在字符串中指定的值；他不会改变调用它的字符串，返回的是新字符串；
- 例子
```js
	str = "Please visit Microsoft!";
	var n = str.replace("Microsoft", "W3School");
	console.log(n);//结果为W3School
```
- 如果需要执行大小写的替换，需要使用正则表达式/i；
- 例子
```js
	str = "Please visit Microsoft!";
	var n = str.replace(/MICROSOFT/i, "W3School");
	console.log(n);//结果为W3School
```

### 转换为大写和小写
- toUpperCase() 把字符串转换为大写
- 例子
```js
	var text1 = "Hello World!";
	var text2 = text1.toUpperCase();
	console.log(text2);//结果为HELLO WORLD!
```
- toLowerCase() 把字符串转换为小写
- 例子
```js
	var text1 = "Hello World!"; 
	var text2 = text1.toLowerCase(); 
	console.log(text2);//结果为hello world!
```

### 其他
- concat() 连接两个或多个字符串
- 例子
```js
	var text1 = "Hello";
    var text2 = "World";
    text3 = text1.concat(" ",text2);
    console.log(text3);//结果为Hello World!
```
- concat() 方法可用于代替加运算符。
- 例子
```js
	var text = "Hello" + " " + "World!";
	var text = "Hello".concat(" ","World!");
	console.log(text);//结果为Hello World!
```
- trim() 方法删除字符串两端的空白符
- 例子
```js
	var str = "       Hello World!        ";
	var str2 = str.trim();
	console.log(str2);//结果为Hello World!
	//Internet Explorer 8 或更低版本不支持 trim() 方法。
```
### 提取字符串字符
- charAt() 方法返回字符串中指定下标（位置）的字符串;
- 例子
```js
	var str = "HELLO WORLD";
	str.charAt(0); //返回 H
```
- charCodeAt() 方法返回字符串中指定索引的字符 unicode 编码
- 例子
```js
	var str = "HELLO WORLD";
	str.charCodeAt(0);// 返回 72
```

### 把字符串转换为数组
- split() 将字符串转换为数组
- 例子
```js
	var txt = "a,b,c,d,e";// 字符串
    txt.split(",");// 用逗号分隔
    txt.split(" ");// 用空格分隔
    txt.split("|");// 用竖线分隔
```


