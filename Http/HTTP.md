## HTTP

### 定义

1. **概述**
   - HTTP 是一个基于请求/响应模式的、无状态的协议
2. **特点**
   - 支持客户端/服务器模式
   - 简单快捷：客户向服务器请求服务时，只需传送请求方法和路径
   - 灵活：允许传输任意类型的数据对象 正在传输的类型由 Content-Type 加以标记
   - 无连接
   - 无状态

### URL

​	统一资源定位符的简称，是因特网上标准的资源的地址。HTTP 服务器的默认端口是 **80**，这种情况下端口号可以省略，如果使用了别的端口，必须指明

### HTTP请求

​	请求报文分为三个部分：请求行  请求头  请求体

1. 请求行：请求方法、请求地址和协议版本

   **常见的请求方法**

   - GET
   - POST
   - PUT              向指定资源位置上传其最新内容
   - DELETE        请求服务器删除 Request-URI 所标识的资源

2. 请求头：传递一些附加信息，格式为：键: 值

   **请求和响应常见通用的 Header**

   - Content-Type：请求体/响应体的类型 如：text/plain  application/json
   - Accept：说明接收的类型 可以多个值 用  英文逗号  分开
   - Content -length：请求体/响应体的长度 单位字节
   - Content-Encoding：编码格式 如：gzip、deflate
   - Accept-Encoding：告知对方我方接收的Content-Encoding
   - ETag：给当前资源的标识 和Last-Modified、If-None-Match、If-Modified-Since  用于缓存控制
   - Cache-Control：取值一般为 no-cache、max-age=xx xx为整数 表示资源缓存有效期（秒）

   **常见的请求Header**

   - Authorization：用于设置身份认证信息
   - User-Agent：用户标识 如：OS和浏览器的类型和版本
   - If-Modified-Since：值为上一次服务器返回的Last-Modified值  用于确定某个资源是否被更改过 没有更改过就从缓存中读取
   - If-None-Match：值为上一次服务器返回的ETagzhi 
   - Cookie：已有的Cookie
   - Referer：标识请求引用自哪个地址 比如从 A页面跳转到B页面 值为A页面的地址
   - Host：请求的主机和端口号

   **请求体**

   ​	post 请求方式中的请求参数，以 key = value 形式进行存储，多个请求参数之间用&连接，如果请求当中有请求体，那么在请求头当中的 Content-Length 属性记录的就是该请求体的长度

   - 第一种：移动开发者常见的，请求体是任意类型的，服务器不会解析请求体，请求体的处理需要自己解析，如 POST JSON 的时候就是这类
   - 第二种：第二种和第三种都有固定的格式，是服务器端开发人员最先了解的两种。这里的格式要求就是 URL 中 Query String 的格式要求：多个键值对之间用&连接，键与值之间用=连接，且只能用 ASCII 字符，非 ASCII 字符需使用UrlEncode编码
   - 第三种：请求体被分成多个部分，文件上传 时会被使用，这种格式最先是被用于邮件传输中，每个字段/文件都被 boundary（Content-Type中指定的）分成单独的段，每段以–加 boundary 开头，然后是该段的描述头，描述头之后空一行接内容，请求结束的标识为 boundary 后面加–

   ### HTTP响应

   1. **响应状态行**

      状态码

      - 1XX 		提示信息——表示请求已接收 继续处理
      - 2XX         用于表示请求已被成功接收 理解 接收
      - 3XX         重定向
      - 4XX         客户端错误——请求有语法错误 或请求无法实现
      - 5XX         服务器错误——服务器未能实现合法的请求

   2. **响应头**

      可用于传递一些附加信息

      常见的响应Header

      - Date        服务器的日期
      - Last-Modified     该资源最后被修改的时间
      - Transfer-Encoding     取值一般为chunked 出现在Content-Length不能确定的情况下 表示服务器不知道响应板体的数据大小 一般同时出现Content-Encoding响应头
      - Set-Cookie     设置Cookie
      - Location     重定向到另一个URL
      - Server     后台服务器

   3. 响应体

      网页的正文内容，一般在响应头中会用 Content-Length 来明确响应体的长度，便于浏览器接收，对于大数据量的正文信息，也会使用 chunked 的编码方式

