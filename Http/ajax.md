## Ajax

​	异步的JavaScript和XML

​	asynchronous	javascript	and		XML

- 同步：不是同时进行

- 异步：不是同时进行

​	js(单进程)直接执行主进程,一旦遇到(定时器 事件 ajax promise async await ...)产生一个任务进程,并且向主进程发出通知,当主进程执行完成之后,将任务进程纳入主进程继续执行

​	ajax最早在2005年 由Google 推广 是在浏览器端进行网络编程(发送请求,接收响应的技术方案) 可以通过js直接获取服务端的最新内容,而不需要重新加载页面 让WEB更能接近桌面应用的用户体验

### XMLHttoRequest作用

1. 从服务器获取数据
2. 向服务器提交数据
3. 达到不刷新页面而局部更新

​	

​	var xhr=new XMLHttpRequest();

​	xhr.open(请求方式,url,是否异步)		//开始请求

​	请求方式:	GET	POST

​	默认异步		true

​	xhr.send()							//发送请求

​	

	### readyState

- 0	创建xhr
- 1	建立连接
- 2	接收
- 3	处理
- 4	完成

​		

### status：状态码

- 1XX
- 2XX		200(新请求成功)	cache
- 3XX		304(请求的页面没有变化	请求客户端缓存)
- 4XX		404
- 5XX		500	501	服务器错误

### 数据格式

- text		responseText

- xml		responseXML

  

浏览器向服务端发送请求的方式

​		地址栏输入	回车		刷新

​		特定的元素

​		表单

### 生命周期		

创建xhr

回调函数		xhr.onreadystatechange=callback

建立连接		xhr.open()

发送数据		xhr.send()

解析数据

ajax生命周期

0	创建xhr

1	初始化请求 发送请求	xhr.send()

2	准备接受数据

3	解析数据

4	解析完成



get:数据传输 (参数) 直接通过url,

post:传参数,通过send(传参)

中断ajax请求	xhr.abort();

Ajax的状态码 || 生命周期

​	0	请求初始化	

​			代理被创建 但是open未调用

​	1	服务器连接已建立

​			open已经调用	建立连接

​	2	请求已接收

​			send方法已被调用 可获取状态行和响应头

​	3	请求处理中

​			响应下载中	可能有部分数据

​	4	请求已完成	响应已就绪

​			响应下载完成	数据完整

 监听状态码

​		onreadystatechange		2 3 4

​			onload				3

get	post

get		相对慢	

​		相对不安全	

​		数据小(4kb)	

​		url地址上

​		

post	相对快	

​		安全性高		

​		数据多		

​		单独数据包

​		需要设置请求头

getAllResponseHeaders （）	获取状态行和响应头

getResponseHeader （）获取指定状态行和响应头